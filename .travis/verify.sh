#!/bin/bash

set -o errexit

install_demo(){
    ./bin/demosite.sh
}

verify_lsws(){
    curl -sIk http://localhost:7090/ | grep -i LiteSpeed
    if [ ${?} = 0 ]; then
        echo '[O]  https://localhost:7090/'
    else
        echo '[X]  https://localhost:7090/'
        exit 1
    fi          
}    

verify_page(){
    curl -sIk http://localhost:8082/ | grep -i WordPress
    if [ ${?} = 0 ]; then
        echo '[O]  http://localhost:8082/' 
    else
        echo '[X]  http://localhost:8082/'
        exit 1
    fi        
    curl -sIk https://localhost:8445/ | grep -i WordPress
    if [ ${?} = 0 ]; then
        echo '[O]  https://localhost:8445/' 
    else
        echo '[X]  https://localhost:8445/'
        exit 1
    fi       
}

verify_phpadmin(){
    curl -sIk http://localhost:8083/ | grep -i phpMyAdmin
    if [ ${?} = 0 ]; then
        echo '[O]  http://localhost:8083/' 
    else
        echo '[X]  http://localhost:8083/'
        exit 1
    fi      
    curl -sIk https://localhost:8446/ | grep -i phpMyAdmin
    if [ ${?} = 0 ]; then
        echo '[O]  http://localhost:8446/' 
    else
        echo '[X]  http://localhost:8446/'
        exit 1
    fi      
}

main(){
    verify_lsws
    verify_phpadmin
    install_demo
    verify_page
}
main